<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 1),
                'name' => env('PLAN_NAME_1', "Metafields Configuration"),
                'price' => env('PLAN_PRICE_1', 18.00),
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Metafields Configuration"),
                'trial_days' => env('TRIAL_DAY', 14),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
        ]);
    }
}
