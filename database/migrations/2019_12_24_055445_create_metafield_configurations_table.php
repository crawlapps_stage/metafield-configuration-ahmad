<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetafieldConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metafield_configurations', function (Blueprint $table) {
            $table->char('id',36);
            $table->primary(['id']);
            $table->integer('shop_id')->unsigned();
            $table->string('label')->nullable();
            $table->string('namespace')->nullable();
            $table->string('key')->nullable();
            $table->string('type')->nullable();
            $table->string('resource_type')->nullable();
            $table->integer('sort_order')->nullable();
            $table->timestamps();

            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metafield_configurations');
    }
}
